from django.shortcuts import render
from django.http import JsonResponse,HttpResponse

# Create your views here.
def index(request):
      return HttpResponse("Hello World")

def doAdd(request,aValue , bValue):
    msg={"a": aValue,"b": bValue,"hasil":"Nilai a dan b yang diberikan harus berupa integer"}
    try:
        sum=int(aValue)+int(bValue)
        msg["hasil"]=sum
    except:
        pass
    return JsonResponse(msg)        
