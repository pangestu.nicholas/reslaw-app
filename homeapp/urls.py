from django.urls import path
from . import views
from django.conf import settings

app_name = 'homeapp'

urlpatterns = [
      path('', views.index, name='index'),
      path('add/a<aValue>&b<bValue>/', views.doAdd, name='addition'),
      
  ]

if settings.DEBUG:
    urlpatterns += [
]